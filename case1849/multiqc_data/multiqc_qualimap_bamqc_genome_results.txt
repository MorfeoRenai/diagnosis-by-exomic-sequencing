Sample	bam_file	total_reads	mapped_reads	mapped_bases	sequenced_bases	mean_insert_size	median_insert_size	mean_mapping_quality	general_error_rate	mean_coverage	percentage_aligned
child	case1849/child.bam	2973585.0	2968953.0	139913081.0	139900533.0	0.0	0.0	37.2423	0.004	24.1403	99.84422843133794
father	case1849/father.bam	2190593.0	2188247.0	158335244.0	158313490.0	0.0	0.0	37.7508	0.0019	27.3188	99.89290571091938
mother	case1849/mother.bam	2050307.0	2046412.0	152176358.0	152152886.0	0.0	0.0	37.7474	0.002	26.2561	99.81002844939808
