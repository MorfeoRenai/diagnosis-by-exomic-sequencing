for case in ../case*
do
    bedtools genomecov -ibam $case/father.bam -bg -trackline -trackopts "name=father" -max 100 > $case/father.bg
    bedtools genomecov -ibam $case/mother.bam -bg -trackline -trackopts "name=father" -max 100 > $case/mother.bg
    bedtools genomecov -ibam $case/child.bam  -bg -trackline -trackopts "name=father" -max 100 > $case/child.bg
done
