for n in 1677 1686 1789 1741 1647 # these are MY cases
do
    # for loop copies the case files in neat directories	
	mkdir case${n}
	cp /home/BCG2023_genomics_exam/case${n}_child.fq.gz ./case${n}
	cp /home/BCG2023_genomics_exam/case${n}_mother.fq.gz ./case${n}
	cp /home/BCG2023_genomics_exam/case${n}_father.fq.gz ./case${n}
done



exam=/home/BCG2023_genomics_exam/ # path to the directory with universe.fasta
bedfile=${exam}*bed               # path to the .bed file, our target regions



echo Path to exam directory: $exam
echo Target regions in form of a .bed file: $bedfile



for case in case*
do
    # for loop iterates through cases directories
	echo -e "\n\n\n\n\nSTARTING A NEW CASE: $case #######################################"
	echo -e "#####################################################################\n\n\n"
	
	for file in $(ls ${case} | grep "fq.gz")
	do
        # for loop iterates through fastq files in the case directory
        
		name=${file: 9}
        	name=${name:: -6} # by name I mean child or father or mother

		echo -e "NOW ALIGNING $file -----------------------------------------"
		bowtie2 -U $case/$file  \
		    --rg-id ${name:: 1}	\
		    --rg "SM:${name}"	\
		    --met 30		\
		    -x ${exam}uni | samtools view -Sb | samtools sort -o $case/$name.bam
		samtools index $case/$name.bam

		echo -e "NOW RUNNING STATISTICS SOFTWARE ON $name.bam ----------------------"
		fastqc --format bam $case/$name.bam
		qualimap bamqc -bam $case/$name.bam --feature-file ./*bed --outdir $case/$name

	done

	echo -e "NOW DOING VARIANT CALLING WITH freebayes ---------------------------------"
	freebayes --fasta-reference ${exam}universe.fasta --targets ${bedfile} --min-mapping-quality 20 --min-base-quality 10 --mismatch-base-quality-threshold 10 --min-alternate-count 5 --min-coverage 10     $case/child.bam $case/father.bam $case/mother.bam > $case/$case.vcf 

	echo -e "NOW RUNNING multiqc FOR FINAL REPORT -------------------------------------"
	multiqc --outdir $case/ $case/
done

