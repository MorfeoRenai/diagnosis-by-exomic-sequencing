# Autosomic Dominant
for n in 1641 1677 1681 1686 1849
do
    grep "#" ../case$n/case$n.vcf > ../case$n/case$n.candidates.vcf
    grep "0/0.*0/0.*0/1" ../case$n/case$n.vcf >> ../case$n/case$n.candidates.vcf
done

# Autosomic Recessive
for n in 1647 1654 1661 1741 1789
do
    grep "#" ../case$n/case$n.vcf > ../case$n/case$n.candidates.vcf
    grep "0/1.*0/1.*1/1" ../case$n/case$n.vcf >> ../case$n/case$n.candidates.vcf
    grep "1/1.*1/1.*2/2" ../case$n/case$n.vcf >> ../case$n/case$n.candidates.vcf
done
