# solution found in https://www.biostars.org/p/9526333/#9526400

awk 'BEGIN {OFS="\t"}{$4=0; $5=0; $6=0; print $0}' \
    /home/BCG2023_genomics_exam/exons16Padded_sorted.bed > ./exons16Padded_sorted.bed
